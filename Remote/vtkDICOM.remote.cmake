#
# Dicom Classes
#

vtk_fetch_module(vtkDICOM
  "Dicom classes and utilities"
  GIT_REPOSITORY https://github.com/dgobbi/vtk-dicom
  # vtk-dicom release 8.9.10
  GIT_TAG faf41f35652fcdd66038e623dff5fbc748ccf15b
  )
